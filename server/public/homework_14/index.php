<?php

interface canPay {
    function pay();
}

abstract class PaymentMethod implements canPay {

    function pay()
    {
        echo 'I pay by ' . strtolower(get_class($this)) . ' for the month of the course.';
        echo '<br>' . $this->paymentInfo() . ' (' . $this->cardtype . ')' . '<br>';
    }

    abstract protected function paymentInfo();
}

class Cash extends PaymentMethod {

    protected function paymentInfo()
    {
        return "cash Info";
    }
}

class  Card extends PaymentMethod {

    protected $cardtype;

    public function __construct($cardtype)
    {
        $this->cardtype = $cardtype;
    }

    protected function paymentInfo()
    {
        return "card Info";
    }
}

class Courses {

    protected $title;

    public function getInfo()
    {
        return ('<br>' . $this->title);
    }
}

class  Course extends Courses {

    protected $manager, $start_date;

    public function __get($prop)
    {
        if (property_exists($this, $prop)) {
            return $this->$prop;
        }
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>Manager: ' . $this->manager . ' | Start Date: ' . $this->start_date;
    }
}

class  CourseGroup extends Course {

    protected $name, $email, $phone;
    private $pay;

    public function __construct($cours_title, $manager, $start_date, $name, $email, $phone)
    {
        $this->title = $cours_title;
        $this->manager = $manager;
        $this->start_date = $start_date;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function __get($prop)
    {
        if (property_exists($this, $prop)) {
            return $this->$prop;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function selectPayment($payment)
    {
        !is_object($payment) ? : $this->pay = $payment;
    }

    public function pay()
    {
        method_exists($this->pay, 'pay') &&
        is_subclass_of($this->pay, "PaymentMethod") ? $this->pay->pay() : "I can't pay (";
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->name . ' | ' . $this->email . ' | ' . $this->phone;
    }

}
// =======================================

$visa = new Card('Visa Debit');
$bob = new CourseGroup('PHP Full Stack', 'Name', '06.10.2021', 'Bob', 'test@test.tt', '0950000000');

echo  $bob->getInfo() . '<hr>';

$bob->selectPayment($visa);
$bob->pay();

echo '<hr>Student: ' . $bob->name;
echo '<br>Start Date: ' . $bob->start_date;

$bob->phone = '095123456789';

echo '<br>Bob\'s New phone number: ' . $bob->phone;
