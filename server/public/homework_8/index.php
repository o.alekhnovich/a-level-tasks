<?php
$registration = false;
$err_pass = false;

/**
 * @param string $pass1
 * @param string $pass2
 * @return bool
 */
$checkInputPassword = fn(string $pass1, string $pass2): bool => $pass1 == $pass2;

if (isset($_POST['name']) && $_POST['name'] != '' &&
    isset($_POST['lastname']) && $_POST['lastname'] != '' &&
    isset($_POST['email']) && $_POST['email'] != '' &&
    isset($_POST['pass1']) && $_POST['pass1'] != '' &&
    isset($_POST['pass2']) && $_POST['pass2'] != ''
    ) {
    if ($checkInputPassword($_POST['pass1'], $_POST['pass2'])) {
        $registration = true;
    } else {
        $err_pass = true;
    }
}

include 'head.html';

echo '<body>';
include 'header.html';

if ($registration) {
    include 'account.html';
} else {
    if ($err_pass) {
        echo '<div class="alert alert-danger alert-dismissible fade show mt-5" role="alert">
                <strong>Password mismatch.</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>';
    }
    include 'registration_form.html';
}

echo '</body>';

include 'footer.html';
