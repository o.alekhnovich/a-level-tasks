<?php

interface canPay {
    function pay();
}

abstract class PaymentMethod implements canPay {
    function pay()
    {
        echo 'I pay by ' . strtolower(get_class($this)) . ' for the month of the course.';
        echo '<br>' . $this->paymentInfo() . '<br>';
    }
    abstract protected function paymentInfo();
}

class Cash extends PaymentMethod {
    protected function paymentInfo()
    {
        return "cash Info";
    }
}
class  Card extends PaymentMethod {
    protected function paymentInfo()
    {
        return "card Info";
    }
}

class Courses {

    protected $title;

    public function set_title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getInfo()
    {
        return ('<br>' . $this->title);
    }
}

class  Course extends Courses {

    protected $manager, $start_date;

    public function set_manager($manager)
    {
        $this->manager = $manager;
        return $this;
    }
    public function get_manager()
    {
        return $this->manager;
    }

    public function set_start_date($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }
    public function get_start_date()
    {
        return $this->start_date;
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>Manager: ' . $this->get_manager() . ' | Start Date: ' . $this->get_start_date();
    }
}

class  CourseGroup extends Course {

    protected $name, $email, $phone;
    private $pay;

    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }
    public function get_name()
    {
        return $this->name;
    }

    public function set_email($email)
    {
        $this->email = $email;
        return $this;
    }
    public function get_email()
    {
        return $this->email;
    }

    public function set_phone($phone)
    {
        $this->phone = $phone;
        return $this;
    }
    public function get_phone()
    {
        return $this->phone;
    }

    public function selectPayment($payment)
    {
        !is_object($payment) ? : $this->pay = $payment;
    }
    public function pay()
    {
        method_exists($this->pay, 'pay') &&
        is_subclass_of($this->pay, "PaymentMethod") ? $this->pay->pay() : "I can't pay (";
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->get_name() . ' | ' . $this->get_email() . ' | ' . $this->get_phone();
    }

}

$visa = new Card();
$bob = new CourseGroup();
$bob
    ->set_title('PHP Full Stack')
    ->set_manager('Name')
    ->set_start_date('06.10.2021')
    ->set_name('Bob')
    ->set_email('test@test.tt')
    ->set_phone('0950000000');
echo  $bob->getInfo() . '<hr>';

$bob->selectPayment($visa);
$bob->pay();
