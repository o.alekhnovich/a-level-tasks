<?php

class Courses {

    protected $title;

    public function set_title($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getInfo()
    {
        return ('<br>' . $this->title);
    }
}

class  Course extends Courses {

    protected $manager, $start_date;

    public function set_manager($manager)
    {
        $this->manager = $manager;
        return $this;
    }
    public function get_manager()
    {
        return $this->manager;
    }

    public function set_start_date($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }
    public function get_start_date()
    {
        return $this->start_date;
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>Manager: ' . $this->get_manager() . ' | Start Date: ' . $this->get_start_date();
    }
}

class  CourseGroup extends Course {

    protected $name, $email, $phone;

    public function set_name($name)
    {
        $this->name = $name;
        return $this;
    }
    public function get_name()
    {
        return $this->name;
    }

    public function set_email($email)
    {
        $this->email = $email;
        return $this;
    }
    public function get_email()
    {
        return $this->email;
    }

    public function set_phone($phone)
    {
        $this->phone = $phone;
        return $this;
    }
    public function get_phone()
    {
        return $this->phone;
    }

    public function getInfo()
    {
        return parent::getInfo() . '<br>' . $this->get_name() . ' | ' . $this->get_email() . ' | ' . $this->get_phone();
    }
}

//$PHPFullStack = new Courses();
//$PHPCourse = new Course();

$oleg = new CourseGroup();

/*
$PHPCourse
    ->set_title('PHP Full Stack')
    ->set_manager('Maneger Name');
*/
$oleg
    ->set_title('PHP Full Stack')
    ->set_manager('Name')
    ->set_start_date('06.10.2021')
    ->set_name('Oleg')
    ->set_email('test@test.tt')
    ->set_phone('0950000000');

echo  $oleg->getInfo();
