-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: kinowiki
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `about` text,
  `dob` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actors`
--

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,'Арнольд','Шварценеггер','американский культурист, предприниматель, киноактёр, продюсер','1947-07-30'),(2,'Сэм','Уортингтон','австралийский актёр','1976-08-02'),(3,'Сэм','Нилл',' новозеландский актёр кино и телевидения','1947-09-14'),(4,'Сигурни','Уивер','американская актриса и продюсер','1949-10-08'),(5,'Роза','Салазар','американская актриса','1985-07-16'),(6,'Лиам','Нисон','британский актёр','1952-06-07'),(7,'Уилл','Феррелл','американский актёр, комик','1967-07-16'),(8,'Хэйли Джоэл','Осмент','американский актёр','1988-04-10'),(9,'Тай','Шеридан','американский актёр','1996-11-11'),(10,'Джон','Траволта','американский актёр, танцор и певец','1954-02-18'),(11,'Ума','Турман','американская актриса','1970-04-29'),(12,'Брэд','Питт','американский актёр и кинопродюсер','1963-12-18'),(13,'Кристиан','Бейл','британо-американский актёр','1974-01-30'),(14,'Леонардо','Ди Каприо','американский актёр, продюсер','1974-11-11'),(15,'Бен','Аффлек','американский актёр кино и телевидения, кинорежиссёр','1972-08-15');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `directors`
--

DROP TABLE IF EXISTS `directors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `directors` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `about` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directors`
--

LOCK TABLES `directors` WRITE;
/*!40000 ALTER TABLE `directors` DISABLE KEYS */;
INSERT INTO `directors` VALUES (1,'Джеймс','Кэмерон','Канадский кинорежиссёр'),(2,'Стивен','Спилберг','Американский кинорежиссёр, продюсер и сценарист.'),(3,'Квентин','Тарантино','Американский кинорежиссёр, сценарист, актёр'),(4,'Кристофер','Нолан','британский и американский кинорежиссёр');
/*!40000 ALTER TABLE `directors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filmography_of_actors`
--

DROP TABLE IF EXISTS `filmography_of_actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filmography_of_actors` (
  `actor_id` int NOT NULL,
  `film_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filmography_of_actors`
--

LOCK TABLES `filmography_of_actors` WRITE;
/*!40000 ALTER TABLE `filmography_of_actors` DISABLE KEYS */;
INSERT INTO `filmography_of_actors` VALUES (1,1);
/*!40000 ALTER TABLE `filmography_of_actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `films` (
  `id` int NOT NULL AUTO_INCREMENT,
  `film_title` varchar(255) NOT NULL,
  `year` int NOT NULL,
  `film_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films`
--

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films` DISABLE KEYS */;
INSERT INTO `films` VALUES (1,'Терминатор',1984,'описание'),(2,'Аватар',2010,'описание'),(3,'Парк Юрского периода',1993,'описание'),(4,'Чужие',1986,'описание'),(5,'Правдивая ложь',1994,'описание'),(6,'Алита: Боевой ангел',2019,'описание'),(7,'«Список Шиндлера»',1993,'описание'),(8,'«Затерянный мир»',1997,'описание'),(9,'«Искусственный разум»',2001,'описание'),(10,'«Первому игроку приготовиться»',2019,'описание'),(11,'Криминальное чтиво',1994,'описание'),(12,'Убить Билла. Фильм 1',2003,'описание'),(13,'Убить Билла. Фильм 2',2004,'описание'),(14,'Бесславные ублюдки',2009,'описание'),(15,'Бэтмен: Начало',2005,'описание'),(16,'Начало',2010,'описание'),(17,'Бэтмен против Супермена: На заре справедливости',2016,'описание');
/*!40000 ALTER TABLE `films` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-27 11:30:57
