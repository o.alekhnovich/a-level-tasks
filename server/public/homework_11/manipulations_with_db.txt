Task 2. FOR MANIPULATION WITH DB I USE:
=======================================

ALTER TABLE films DROP COLUMN director_id;


INSERT INTO directors(firstname, lastname, about) VALUES
> ('Квентин', 'Тарантино', 'Американский кинорежиссёр, сценарист, актёр'),
> ('Кристофер', 'Нолан', 'британский и американский кинорежиссёр');


SELECT * FROM directors;


select film_title as title, year from films where year > 2000;


select film_title as title, year from films order by year desc;


select film_title as title, year from films where year < 2000 order by year desc;


select film_title as title, year from films where year < 2000 order by year desc limit 3;


select film_title as title, year from films where year < 2010 and year > 1998 order by year desc limit 3;



