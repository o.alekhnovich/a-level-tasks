<?php

echo '<h1>My Books</h1>';

class Books {

    protected $title, $author, $year;

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function getInfo($con)
    {
        $count = 1;
        foreach($con->query('SELECT * FROM books') as $row) {
            echo $count . ') ' . $row[1] . ' (Автор: ' . $row[2] . '. Год: ' . $row[3] . ')<br>';
            $count++;
        }
    }

    public function addToDB($con)
    {
        $con->query("INSERT INTO books(title, author, year) VALUES('$this->title', '$this->author', '$this->year')");
    }
}

class DBCon {

    static $instance;

    private function __clone() {}
    private function __wakeup() {}

    protected  function __construct()
    {
        try {
            $this->con = new PDO ('mysql:host=wpbit_mysql;port=3306;charset=UTF8;dbname=wpbit_mysql', 'root', 'password');
        } catch (PDOException $e) {
            echo "Error!: " . $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }
        return  self::$instance->con;
    }
}

// ==========================

$books_info = new Books();

$book = new Books();
$book->title = 'Book Title';
$book->author = 'Author Name';
$book->year = '1900';
$book->addToDB(DBCon::getInstance());

echo $books_info->getInfo(DBCon::getInstance());
