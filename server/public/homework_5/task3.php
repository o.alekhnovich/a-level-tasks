<?php
/**
 * @param array $arr
 * @return  boolean
 */
function dd(array $arr): bool
{
    // phpcs:disable
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    // phpcs:enable

    return true;
}


// 1. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// Развернуть этот массив в обратном направлении
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

// WHILE
// count elements of array
$arr_count = 0;
while ($arr_count >= 0) {
    if (isset($names[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}
// new array for revers
while ($arr_count-- != 0) {
    $names_reverse[] = $names[$arr_count];
}
dd($names_reverse);

// DO WHILE
// count elements of array
$arr_count = 0;
do {
    if (isset($names[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
// new array for revers
do {
    $names_reverse_1[] = $names[--$arr_count];
} while ($arr_count != 0);
dd($names_reverse_1);

// FOR
// count elements of array
for ($i = 0; $i >= 0; ++$i) {
    if (isset($names[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}
// new array for revers
for ($i = $arr_count; $i != -1; --$i) {
    $names_reverse_2[] = $names[$i];
}
dd($names_reverse_2);

// FOREACH
// count elements of array
$count = -1;
foreach ($names as $name) {
    $count++;
}
// new array for revers
foreach ($names as $name) {
    $names_reverse_3[] = $names[$count];
    $count--;
}
dd($names_reverse_3);

// 2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Развернуть этот массив в обратном направлении
$numbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];

// WHILE
// count elements of array
$arr_count = 0;
while ($arr_count >= 0) {
    if (isset($numbers[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}
// new array for revers
while ($arr_count-- != 0) {
    $numbers_reverse[] = $numbers[$arr_count];
}
dd($numbers_reverse);

// DO WHILE
// count elements of array
$arr_count = 0;
do {
    if (isset($numbers[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
// new array for revers
do {
    $numbers_reverse_1[] = $numbers[--$arr_count];
} while ($arr_count != 0);
dd($numbers_reverse_1);

// FOR
// count elements of array
for ($i = 0; $i >= 0; ++$i) {
    if (isset($numbers[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}
// new array for revers
for ($i = $arr_count; $i != -1; --$i) {
    $numbers_reverse_2[] = $numbers[$i];
}
dd($numbers_reverse_2);

// FOREACH
// count elements of array
$count = -1;
foreach ($numbers as $number) {
    $count++;
}
// new array for revers
foreach ($numbers as $number) {
    $numbers_reverse_3[] = $numbers[$count];
    $count--;
}
dd($numbers_reverse_3);

// 3. Дана строка
// let str = 'Hi I am ALex'
// развенуть строку в обратном направлении.
$str = 'Hi I am ALex';

// WHILE
// count elements of array
$arr_count = 0;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}
// new array for revers
$str_new = null;
while ($arr_count-- != 0) {
    $str_new .= $str[$arr_count];
}
echo $str_new . '<br>';

// DO WHILE
// count elements of array
$arr_count = 0;
do {
    if (isset($str[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
// new array for revers
$str_new = null;
do {
    $str_new .= $str[--$arr_count];
} while ($arr_count != 0);
echo $str_new . '<br>';

// FOR
// count elements of array
for ($i = 0; $i >= 0; ++$i) {
    if (isset($str[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}
// new array for revers
$str_new = null;
for ($i = $arr_count; $i != -1; --$i) {
    $str_new .= $str[$i];
}
echo $str_new . '<br>';

// FOREACH
// need to make array (ex.: via WHILE)
$arr_count = 0;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        $arr_str[] = $str[$arr_count];
        $arr_count++;
    } else {
        break;
    }
}
// count elements of array
$count = -1;
foreach ($arr_str as $value) {
    $count++;
}
// new array for revers
$str_new = null;
foreach ($arr_str as $value) {
    $str_new .= $arr_str[$count];
    $count--;
}
echo $str_new . '<br>';

// 4. Дана строка. готовую функцию toUpperCase() or tolowercase()
// let str = 'Hi I am ALex'
// сделать ее с с маленьких букв
$str = 'Hi I am ALex';
$abcABC = [
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
    'f' => 'F',
    'g' => 'G',
    'h' => 'H',
    'i' => 'I',
    'j' => 'J',
    'k' => 'K',
    'l' => 'L',
    'm' => 'M',
    'n' => 'N',
    'o' => 'O',
    'p' => 'P',
    'q' => 'Q',
    'r' => 'R',
    's' => 'S',
    't' => 'T',
    'u' => 'U',
    'v' => 'V',
    'w' => 'W',
    'x' => 'X',
    'y' => 'Y',
    'z' => 'Z',
];
$ABCabc = [
    'A' => 'a',
    'B' => 'b',
    'C' => 'c',
    'D' => 'd',
    'E' => 'e',
    'F' => 'f',
    'G' => 'g',
    'H' => 'h',
    'I' => 'i',
    'J' => 'j',
    'K' => 'k',
    'L' => 'l',
    'M' => 'm',
    'N' => 'n',
    'O' => 'o',
    'P' => 'p',
    'Q' => 'q',
    'R' => 'r',
    'S' => 's',
    'T' => 't',
    'U' => 'u',
    'V' => 'v',
    'W' => 'w',
    'X' => 'x',
    'Y' => 'y',
    'Z' => 'z',
];

// WHILE
$arr_count = 0;
$str_new = null;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        if (isset($ABCabc[$str[$arr_count]])) {
            $str_new .= $ABCabc[$str[$arr_count]];
        } else {
            $str_new .= $str[$arr_count];
        }
        $arr_count++;
    } else {
        break;
    }
}
echo $str_new . '<br>';

// DO WHILE
$arr_count = 0;
$str_new = null;
do {
    if (isset($str[$arr_count])) {
        if (isset($ABCabc[$str[$arr_count]])) {
            $str_new .= $ABCabc[$str[$arr_count]];
        } else {
            $str_new .= $str[$arr_count];
        }
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
echo $str_new . '<br>';

// FOR
$str_new = null;
for ($i = 0; $i >= 0; $i++) {
    if (isset($str[$i])) {
        if (isset($ABCabc[$str[$i]])) {
            $str_new .= $ABCabc[$str[$i]];
        } else {
            $str_new .= $str[$i];
        }
    } else {
        break;
    }
}
echo $str_new . '<br>';

// FOREACH
// need to make array (ex.: via WHILE)
$arr_count = 0;
$arr_str = null;
$str_new = null;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        $arr_str[] = $str[$arr_count];
        $arr_count++;
    } else {
        break;
    }
}
foreach ($arr_str as $value) {
    if (isset($ABCabc[$value])) {
        $str_new .= $ABCabc[$value];
    } else {
        $str_new .= $value;
    }
}
echo $str_new . '<br>';

// 5. Дана строка
// let str = 'Hi I am ALex'
// сделать все буквы большие
// WHILE
$arr_count = 0;
$str_new = null;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        if (isset($abcABC[$str[$arr_count]])) {
            $str_new .= $abcABC[$str[$arr_count]];
        } else {
            $str_new .= $str[$arr_count];
        }
        $arr_count++;
    } else {
        break;
    }
}
echo $str_new . '<br>';

// DO WHILE
$arr_count = 0;
$str_new = null;
do {
    if (isset($str[$arr_count])) {
        if (isset($abcABC[$str[$arr_count]])) {
            $str_new .= $abcABC[$str[$arr_count]];
        } else {
            $str_new .= $str[$arr_count];
        }
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
echo $str_new . '<br>';

// FOR
$str_new = null;
for ($i = 0; $i >= 0; $i++) {
    if (isset($str[$i])) {
        if (isset($abcABC[$str[$i]])) {
            $str_new .= $abcABC[$str[$i]];
        } else {
            $str_new .= $str[$i];
        }
    } else {
        break;
    }
}
echo $str_new . '<br>';

// FOREACH
// need to make array (ex.: via WHILE)
$arr_count = 0;
$arr_str = null;
$str_new = null;
while ($arr_count >= 0) {
    if (isset($str[$arr_count])) {
        $arr_str[] = $str[$arr_count];
        $arr_count++;
    } else {
        break;
    }
}
foreach ($arr_str as $value) {
    if (isset($abcABC[$value])) {
        $str_new .= $abcABC[$value];
    } else {
        $str_new .= $value;
    }
}
echo $str_new . '<br>';

// 6. Дана строка
// let str = 'Hi I am ALex'
// развернуть ее в обратном направлении
echo 'задача №6 - дубль задачи №3 <br>';

// 7. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с маленькой
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

// WHILE
$count = 0;
while ($count >= 0) {
    if (isset($names[$count])) {
        $count_letters = 0;
        while ($count_letters >= 0) {
            if (isset($names[$count][$count_letters])) {
                if (isset($ABCabc[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $ABCabc[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        }
    } else {
        break;
    }
    $count++;
}
dd($names);

// DO WHILE
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
do {
    if (isset($names[$count])) {
        $count_letters = 0;
        do {
            if (isset($names[$count][$count_letters])) {
                if (isset($ABCabc[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $ABCabc[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        } while ($count_letters >= 0);
    } else {
        break;
    }
    $count++;
} while ($count >= 0);
dd($names);

// FOR
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
for ($i = 0; $i >= 0; $i++) {
    if (isset($names[$i])) {
        for ($x = 0; $x >= 0; $x++) {
            if (isset($names[$i][$x])) {
                if (isset($ABCabc[$names[$i][$x]])) {
                    $names[$i][$x] = $ABCabc[$names[$i][$x]];
                }
            } else {
                break;
            }
        }
    } else {
        break;
    }
}
dd($names);

// FOREACH
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($names as $key => $name) {
    $name_new = null;
    $arr_name = null;
    $arr_count = 0;
    while ($arr_count >= 0) {
        if (isset($name[$arr_count])) {
            $arr_name[] = $name[$arr_count];
            $arr_count++;
        } else {
            break;
        }
    }
    foreach ($arr_name as $value) {
        if (isset($ABCabc[$value])) {
            $name_new .= $ABCabc[$value];
        } else {
            $name_new .= $value;
        }
    }
    $names_new[$key] = $name_new;
}
dd($names_new);

// 8. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с большой
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];

// WHILE
$count = 0;
while ($count >= 0) {
    if (isset($names[$count])) {
        $count_letters = 0;
        while ($count_letters >= 0) {
            if (isset($names[$count][$count_letters])) {
                if (isset($abcABC[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $abcABC[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        }
    } else {
        break;
    }
    $count++;
}
dd($names);

// DO WHILE
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
$count = 0;
do {
    if (isset($names[$count])) {
        $count_letters = 0;
        do {
            if (isset($names[$count][$count_letters])) {
                if (isset($abcABC[$names[$count][$count_letters]])) {
                    $names[$count][$count_letters] = $abcABC[$names[$count][$count_letters]];
                }
            } else {
                break;
            }
            $count_letters++;
        } while ($count_letters >= 0);
    } else {
        break;
    }
    $count++;
} while ($count >= 0);
dd($names);

// FOR
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
for ($i = 0; $i >= 0; $i++) {
    if (isset($names[$i])) {
        for ($x = 0; $x >= 0; $x++) {
            if (isset($names[$i][$x])) {
                if (isset($abcABC[$names[$i][$x]])) {
                    $names[$i][$x] = $abcABC[$names[$i][$x]];
                }
            } else {
                break;
            }
        }
    } else {
        break;
    }
}
dd($names);

// FOREACH
$names = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
foreach ($names as $key => $name) {
    $name_new = null;
    $arr_name = null;
    $arr_count = 0;
    while ($arr_count >= 0) {
        if (isset($name[$arr_count])) {
            $arr_name[] = $name[$arr_count];
            $arr_count++;
        } else {
            break;
        }
    }
    foreach ($arr_name as $value) {
        if (isset($abcABC[$value])) {
            $name_new .= $abcABC[$value];
        } else {
            $name_new .= $value;
        }
    }
    $names_new[$key] = $name_new;
}
dd($names_new);

// 9. Дано число
// let num = 1234678
// развернуть ее в обратном направлении
$num = 1234678;
$str_num = (string)$num;

// WHILE
// count elements of array
$arr_count = 0;
$num_new = null;
while ($arr_count >= 0) {
    if (isset($str_num[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
}
// new array for revers
while ($arr_count-- != 0) {
    $num_new .= $str_num[$arr_count];
}
echo (integer)$num_new . '<br>';

// DO WHILE
// count elements of array
$arr_count = 0;
$num_new = null;
do {
    if (isset($str_num[$arr_count])) {
        $arr_count++;
    } else {
        break;
    }
} while ($arr_count >= 0);
// new array for revers
do {
    $num_new .= $str_num[--$arr_count];
} while ($arr_count != 0);
echo (integer)$num_new . '<br>';

// FOR
// count elements of array
$num_new = null;
for ($i = 0; $i >= 0; ++$i) {
    if (isset($str_num[$i])) {
        $arr_count = $i;
    } else {
        break;
    }
}
// new array for revers
$str_new = null;
for ($i = $arr_count; $i != -1; --$i) {
    $num_new .= $str_num[$i];
}
echo (integer)$num_new . '<br>';

// FOREACH
// need to make array (ex.: via WHILE)
$arr_count = 0;
$num_new = null;
while ($arr_count >= 0) {
    if (isset($str_num[$arr_count])) {
        $arr_str[] = $str_num[$arr_count];
        $arr_count++;
    } else {
        break;
    }
}
// count elements of array
$count = -1;
foreach ($arr_str as $value) {
    $count++;
}
// new array for revers
$str_new = null;
foreach ($arr_str as $value) {
    $num_new .= $arr_str[$count];
    $count--;
}
echo (integer)$num_new . '<br>';

// 10. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// отсортируй его в порядке убывания
$num_arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];

// WHILE
$count_arr_elements = 0;
$count = 0;
$count_all_num = 0;
$num_arr_new = null;
// count elements of array
while ($count_arr_elements >= 0) {
    if (isset($num_arr[$count_arr_elements])) {
        $count_arr_elements++;
    } else {
        break;
    }
}
// sort array
while ($count < $count_arr_elements) {
    $count_arr = 0;
    while ($count_arr < $count_arr_elements) {
        if ($num_arr[$count_arr] == $count_all_num) {
            $num_arr_new[] = $num_arr[$count_arr];
            $count++;
        }
        $count_arr++;
    }
    $count_all_num++;
}
// new array for revers
while ($count-- != 0) {
    $num_arr_reverse[] = $num_arr_new[$count];
}
dd($num_arr_reverse);

// DO WHILE
$count_arr_elements = 0;
$count = 0;
$count_all_num = 0;
$num_arr_new = null;
$num_arr_reverse = null;
// count elements of array
do {
    if (isset($num_arr[$count_arr_elements])) {
        $count_arr_elements++;
    } else {
        break;
    }
} while ($count_arr_elements >= 0);
// sort array
do {
    $count_arr = 0;
    do {
        if ($num_arr[$count_arr] == $count_all_num) {
            $num_arr_new[] = $num_arr[$count_arr];
            $count++;
        }
        $count_arr++;
    } while ($count_arr < $count_arr_elements);
    $count_all_num++;
} while ($count < $count_arr_elements);
// new array for revers
do {
    $num_arr_reverse[] = $num_arr_new[--$count];
} while ($count != 0);
dd($num_arr_reverse);

// FOR
// count elements of array
$count = 0;
$arr_count = 0;
$num_arr_new = null;
$num_arr_reverse = null;
for ($i = 0; $i >= 0; ++$i) {
    if (isset($num_arr[$i])) {
        $arr_count++;
    } else {
        break;
    }
}
// sort array
for ($i = 0; $i >= 0; ++$i) {
    if ($count < $arr_count) {
        for ($x = 0; $x < $arr_count; ++$x) {
            if ($num_arr[$x] == $i) {
                $num_arr_new[] = $num_arr[$x];
                $count++;
            }
        }
    } else {
        break;
    }
}
// new array for revers
for ($i = --$arr_count; $i >= 0; --$i) {
    $num_arr_reverse[] = $num_arr_new[$i];
}
dd($num_arr_reverse);

// FOREACH
$count = 0;
$count_arr = 0;
$count_all = 0;
$num_arr_new = null;
$num_arr_reverse = null;
foreach ($num_arr as $value) {
    $count_arr++;
}
while ($count < $count_arr) {
    foreach ($num_arr as $value) {
        if ($value == $count_all) {
            $num_arr_new[] = $value;
            $count++;
        }
    }
    $count_all++;
}
// new array for revers
foreach ($num_arr as $value) {
    $num_arr_reverse[] = $num_arr_new[--$count_arr];
}
dd($num_arr_reverse);
