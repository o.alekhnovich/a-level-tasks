<?php

echo '<h1>Угадай число</h1>';

if (isset($_POST['number']) && $_POST['number'] != '') {
    $rand_num = rand(5, 8);
    if ($_POST['number'] == $rand_num) {
        echo '<p>Вы УГАДАЛИ!!!</p><p><a href="index.php">Играть еще</p>';
    } else {
        if ($_POST['number'] < 5) {
            echo '<p>Число маленькое :(</p>';
        } elseif ($_POST['number'] > 8) {
            echo '<p>Число большое :(</p>';
        } else {
            echo '<p>Попробуйте еще:</p>';
            include 'form.html';
        }
    }
} else {
    echo '<p>Введите число:</p>';
    include 'form.html';
}
