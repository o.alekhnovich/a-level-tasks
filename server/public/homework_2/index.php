<?php
$name = 'Oleg A';
$age = 37;
$pi = 3.14;

$arr_1 = ['alex', 'vova', 'tolya'];
$arr_2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
$arr_3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', 'mila']]];
$arr_4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', 'mila']];

// for terminal & html
echo 'My name is ' . $name . '<br>';
echo 'Age: '. $age . '<br>';
echo 'Pi: ' . $pi . '<br>';

print('<br>My name is ' . $name . '<br>');
print('<br>Age: ' . $age . '<br>');
print('<br>Pi: ' . $pi . '<br>');

// Notice: Array to string conversion in...
print($arr_1);
print($arr_2);
print($arr_3);
print($arr_4);

echo '<pre>';
// phpcs:disable
print_r($arr_1);
print_r($arr_2);
print_r($arr_3);
print_r($arr_4);
// phpcs:enable
echo '</pre>';
