<?php
/**
 * @param array $arr
 * @return boolean
 */
function dd(array $arr): bool
{
    // phpcs:disable
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    // phpcs:enable

    return true;
}

// 1. Найти минимальное и максимальное среди 3 чисел
// 1)
$showMinMaxNum = function (...$numbers) {
    $minMaxNum['min'] = $numbers[0];
    $minMaxNum['max'] = $numbers[0];
    foreach ($numbers as $value) {
        if ($value < $minMaxNum['min']) {
            $minMaxNum['min'] = $value;
        } elseif ($value > $minMaxNum['max']) {
            $minMaxNum['max'] = $value;
        }
    }
    return $minMaxNum;
};
dd($showMinMaxNum(74, 94, 37));

// 2)
$arr = [74, 94, 37];
/**
 * @param array $array
 * @param mixed $fn
 * @return string
 */
function showMinMaxNum($array, $fn): string
{
    $min_num = $array[0];
    $max_num = $array[0];
    foreach ($array as $value) {
        $fn($value, $min_num, $max_num);
    }
    return ('MIN NUM: ' . $min_num . ', MAX NUM: ' . $max_num);
}
$showMinMaxNum = showMinMaxNum($arr, fn($num, &$min_num, &$max_num) => $num < $min_num ? $min_num = $num : ($num > $max_num ? $max_num = $num : false));
echo $showMinMaxNum . '<br>';

// 2. Найти площадь
// 1)
$calcSq = function ($a, $b) {
    return $a * $b;
};
echo 'Площадь равна: ' . $calcSq(43, 56) . '<br>';
// 2)
$calcSq = fn($a, $b) => $a * $b;
echo 'Площадь равна: ' . $calcSq(21, 23) . '<br><hr>';


// 3. Теорема Пифагора
// 1)
$calcPifagor = function ($c, $a, $b) {
    if (($c * $c) == ($a * $a) + ($b * $b)) {
        return true;
    } else {
        return false;
    }
};
echo $calcPifagor(3, 6, 9) ? 'Да, треугольник прямоугольный<br>' : 'Нет, треугольник не прямоугольный<br>';

$findHypotenuse = function ($a, $b) {
    $sum_sq = ($a * $a) + ($b * $b);
    $c = sqrt($sum_sq);
    return $c;
};
echo 'Гипотенуза равна: ' . $findHypotenuse(3, 7) . '<br>';

// 2)
$calcPifagor = fn($c, $a, $b) => ($c * $c) == ($a * $a) + ($b * $b) ? 'Да, треугольник прямоугольный' : 'Нет, треугольник не прямоугольный';
echo $calcPifagor(3, 5, 7) . '<br>';
$findHypotenuse = fn($a, $b) => sqrt(($a * $a) + ($b * $b));
echo 'Гипотенуза равна: ' . $findHypotenuse(3, 7) . '<br><hr>';


// 4. Найти периметр
// 1)
$calcRectanglePerimeter = function ($l, $w) {
    return 2 * ($l + $w);
};
echo 'Периметр прямоугольника равен: ' .  $calcRectanglePerimeter(16, 9) . '<br>';
// 2)
$rectanglePerimeter = fn($l, $w) => 2 * ($l + $w);
echo 'Периметр прямоугольника равен: ' .  $rectanglePerimeter(16, 9) . '<br>';

// 1)
$calcCirclePerimeter = function ($r = '', $d = '') {
    if ($r != '') {
        $P = 2 * (pi() * $r);
    } elseif ($d != '') {
        $P = pi() * $d;
    }
    return $P;
};
echo 'Периметр окружности равен: ' . $calcCirclePerimeter(8, '') . '<br>';
// 2)
$circlePerimeter = fn($r = '', $d = '') => $r != '' ? 2 * (pi() * $r) : ($d != '' ? pi() * $d : false);
echo 'Периметр окружности равен: ' . $circlePerimeter('', 16) . '<br>';

// 1)
$calcTrianglePerimeter = function ($a, $b, $c) {
    return $a + $b + $c;
};
echo 'Периметр треугольника равен: '  . $calcTrianglePerimeter(5, 7, 10) . '<br>';
// 2
$trianglePerimeter = fn($a, $b, $c) => $a + $b + $c;
echo 'Периметр треугольника равен: '  . $trianglePerimeter(5, 7, 10) . '<br>';

// 1)
$calcSquarePerimeter = function ($a) {
    return 4 * $a;
};
echo 'Периметр квадрата равен: ' . $calcSquarePerimeter(23) . '<br>';
// 2)
$SquarePerimeter = fn($a) => 4 * $a;
echo 'Периметр квадрата равен: ' . $SquarePerimeter(23) . '<br>';

// 1)
$calcPolygonPerimeter = function ($n, $a) {
    return $n * $a;
};
echo 'Периметр правильного многоугольника равен: ' . $calcPolygonPerimeter(6, 12) . '<br>';
// 2)
$calcPolygonPerimeter = fn($n, $a) => $n * $a;
echo 'Периметр правильного многоугольника равен: ' . $calcPolygonPerimeter(6, 12) . '<hr>';


// 5. Найти дискриминант
// 1)
$arr = [4, 9, 3]; // $a, $b, $c
$discriminantInfo = function ($arr) {
    // D = b2 - 4ac
    $D = ($arr[1] * 2) - (4 * $arr[0] * $arr[2]);
    if ($D < 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней нет';
    }
    if ($D == 0) {
        $resault = 'D = ' . $D . ' -Корень один';
    }
    if ($D > 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней — два';
    }
    return $resault;
};
echo $discriminantInfo($arr) . '<br>';
// 2)
/**
 * @param array $arr
 * @param mixed $fn
 * @return string
 */
function discriminantInfo($arr, $fn): string
{
    // D = b2 - 4ac
    $D = $fn($arr[0], $arr[1], $arr[2]);
    if ($D < 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней нет';
    }
    if ($D == 0) {
        $resault = 'D = ' . $D . ' -Корень один';
    }
    if ($D > 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней — два';
    }
    return $resault;
}
echo discriminantInfo($arr, fn($a, $b, $c) => ($b * 2) - (4 * $a * $c)) . '<hr>';


// 6. Создать только четные числа до 100
// 1)
$getEvenNumbers = function ($max_num) {
    for ($i = 1; $i <= $max_num; $i++) {
        if (($i % 2) == 0) {
            $evenNums[] = $i;
        }
    }
    return $evenNums;
};
dd($getEvenNumbers(100));

// 2)
/**
 * @param integer $max_num
 * @return array
 */
function arrNums($max_num): array
{
    for ($i = 1; $i <= $max_num; $i++) {
        $arr_nums[] = $i;
    }
    return $arr_nums;
}
$getEvenNumbers = array_filter(arrNums(100), fn($num) => ($num % 2) == 0);
dd($getEvenNumbers);


// 7. Создать нечетные числа до 100
// 1)
$getOddNumbers = function ($max_num) {
    for ($i = 1; $i <= $max_num; $i++) {
        if (($i % 2) != 0) {
            $oddNums[] = $i;
        }
    }
    return $oddNums;
};
dd($getOddNumbers(100));

// 2)
$getOddNumbers = array_filter(arrNums(100), fn($num) => ($num % 2) != 0);
dd($getOddNumbers);


// 8. Определите, есть ли в массиве повторяющиеся элементы.
// 1)
$arr = [43, 32, 2, 22, 43, 55, 88, 44, 44, 99, 44, 11, 22];
$findDublicate = function ($array) {
    $count_arr = count($array);
    for ($i = 0; $i < $count_arr; $i++) {
        for ($x = $i; $x < $count_arr; $x++) {
            if ($array[$i] == $array[$x] && $i != $x) {
                $new_arr[] = $array[$i];
            }
        }
    }
    return $new_arr;
};
dd($findDublicate($arr)); //
// 2)
/**
 * @param array   $array
 * @param integer $item
 * @return boolean
 */
function findDublicate($array, $item): bool
{
    $count = 0;
    foreach ($array as $value) {
        if ($value == $item) {
            if ($count > 0) {
                return true;
            }
            $count++;
        }
    }
    return false;
}
$findDublicate = array_filter($arr, fn($item) => findDublicate($arr, $item));
dd($findDublicate); // all dublicate values in the new array
echo '<hr>';

// 9. Сортировка из прошлого задания
$num_arr = [];
for ($i = 0; $i < 20; $i++) {
    $num_arr[] = rand(1, 100);
}
// 1)
$getSortNumArr = function ($array, $sort = '') {
    switch ($sort) {
        case 'asc':
            sort($array);
            break;
        case 'desc':
            rsort($array);
            break;
        default:
            sort($array);
    }
    return $array;
};
dd($getSortNumArr($num_arr, 'desc'));
// 2)
$sort = 'asc';
$getSortNumArr = fn(&$array, $sort) => $sort == 'desc' ? rsort($array) : sort($array);
$getSortNumArr($num_arr, $sort);
dd($num_arr);
