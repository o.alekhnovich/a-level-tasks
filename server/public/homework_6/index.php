<?php
/**
 * @param array $arr
 * @return boolean
 */
function dd(array $arr): bool
{
    // phpcs:disable
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    // phpcs:enable

    return true;
}

// 1. Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand).
// Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные
// индексы. После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.
/**
 * @param integer $len_arr
 * @param integer $start_num
 * @param integer $end_num
 * @return array
 */
function createRandNumArr($len_arr, $start_num, $end_num): array
{
    for ($i = 0; $i < $len_arr; $i++) {
        $num_arr[] = rand($start_num, $end_num);
    }
    return $num_arr;
}

/**
 * @param array $num_arr
 * @return integer
 */
function multiplyEvenNumbers(array $num_arr): int
{
    $new_value = 1;
    foreach ($num_arr as $value) {
        if ($value > 0 && ($value % 2) == 0) {
            $new_value *= $value;
        }
    }
    return $new_value;
}

/**
 * @param array $num_arr
 * @return string
 */
function noneEvenNumbersList(array $num_arr): string
{
    $num_str = null;
    foreach ($num_arr as $value) {
        if ($value > 0 && ($value % 2) == 1) {
            $num_str = $num_str . $value . '<br>';
        }
    }
    return $num_str;
}

$num_arr = createRandNumArr(10, 1, 100);

echo multiplyEvenNumbers($num_arr), '<br>---<br>';
echo noneEvenNumbersList($num_arr) . '<hr>';


// 2. Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.
$num1 = 56;
$num2 = 32;

/**
 * @param integer $x
 * @param integer $y
 * @return integer
 */
function numSum($x, $y): int
{
    return $x + $y;
}

/**
 * @param integer $x
 * @param integer $y
 * @return integer
 */
function numMultiply($x, $y): int
{
    return $x * $y;
}

/**
 * @param integer $x
 * @param integer $y
 * @return integer
 */
function numSumSq($x, $y): int
{
    $x *= $x;
    $y *= $y;
    return $x + $y;
}

echo numSum($num1, $num2) . '<br>';
echo numMultiply($num1, $num2) . '<br>';
echo numSumSq($num1, $num2) . '<hr>';


// 3. Даны три числа. Найдите их среднее арифметическое.
$x = 36;
$y = 63;
$z = 76;

/**
 * @param mixed ...$args
 * @return float
 */
function numAverage(...$args): float
{
    $count = 0;
    $sum = 0;
    foreach ($args as $value) {
        $sum += $value;
        $count++;
    }
    return $sum / $count;
}

echo numAverage($x, $y, $z) . '<hr>';


// 4. Дано число. Увеличьте его на 30%, на 120%.
$num = 432;
/**
 * @param integer $num
 * @param integer $perc
 * @return float
 */
function percentIncrease($num, $perc): float
{
    $num += ($num / 100 * $perc);
    return $num;
}

echo percentIncrease($num, 30) . ' (' . $num . '+30%)<br>';
echo percentIncrease($num, 120) . ' (' . $num . '+120%)<br><hr>';


// 5. Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия),
// вводит количество дней для отдыха и указывает, есть ли у него скидка (чекбокс).
// Вывести стоимость отдыха, которая вычисляется как произведение   количества дней на 400.
// Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия.
// И далее это число уменьшается на 5%, если указана скидка.
$sale_perc = 5;
$_GLOBALS['perc_by_country'] = [
    'turkey' => 0,
    'egypt' => 10,
    'italy' => 12
];

/**
 * @param string        $country
 * @param integer       $days
 * @param float|integer $price
 * @return float
 */
function showTourPrice($country, $days, $price = 400): float
{
    if (isset($perc_by_country[$country]) && $perc_by_country[$country] != 0) {
        $price = $price * $days;
        $price += ($price / 100 * $perc_by_country[$country]);
        return $price;
    } else {
        return $days * $price;
    }
}

/**
 * @param string        $country
 * @param integer       $days
 * @param integer       $sale_perc
 * @param float|integer $price
 * @return float
 */
function showTourSalePrice($country, $days, $sale_perc, $price = 400): float
{
    $standart_price = showTourPrice($country, $days, $price);
    return $standart_price - ($standart_price / 100 * $sale_perc);
}

if (isset($_POST['country']) && isset($_POST['days']) && $_POST['days'] != '') {
    if (isset($_POST['sale'])) {
        echo 'Стоимость Вашего тура: ' . showTourSalePrice($_POST['country'], $_POST['days'], $sale_perc);
    } else {
        echo 'Стоимость Вашего тура: ' . showTourPrice($_POST['country'], $_POST['days']);
    }
}
include 'tour_form.html';


// 6. Пользователь вводит свой имя, пароль, email. Если вся информация указана,
// то показать эти данные после фразы 'Регистрация прошла успешно', иначе сообщить
// какое из полей оказалось не заполненным.
/**
 * @param string $name
 * @param string $email
 * @param string $password
 * @return array
 */
function checkFormErr($name, $email, $password): array
{
    $err = null;
    if ($name != '' && $email != '' && $password != '') {
        return $err;
    } else {
        if ($name == '') {
            $err[] = 'Не заполнено поле: Ваше Имя';
        }
        if ($email == '') {
            $err[] = 'Не заполнено поле: Email';
        }
        if ($password == '') {
            $err[] = 'Не заполнено поле: Пароль';
        }
        return $err;
    }
}

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['pass'])) {
    $form_errors = checkFormErr($_POST['name'], $_POST['email'], $_POST['pass']);
    $reg = true;
} else {
    $form_errors = null;
    $reg = false;
}

if ($reg && $form_errors == null) {
    echo '<h2 style="color: green">Регистрация прошла успешно</h2><p>Ваши данные:</p>' .
    '<p>Имя: ' . $_POST['name'] . '</p>' .
    '<p>Email: ' . $_POST['email'] . '</p>' .
    '<p>Пароль: ' . $_POST['pass'] . '</p>';
} elseif ($reg && $form_errors != null) {
    foreach ($form_errors as $value) {
        echo '<p style="color: red"><small>' . $value . '</small></p>';
    }
}
include 'registration_form.html';

// 7. Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь
// на форме. Если n некорректно, вывести фразу "Bad n".
$text = 'Silence is golden';
$count = false;
$count_num = null;
if (isset($_POST['count']) && $_POST['count'] != '') {
    $count = true;
    $count_num = $_POST['count'];
}
/**
 * @param integer $number
 * @return boolean
 */
function checkNum($number): bool
{
    if ((integer)$number && $number > 0 && $number <= 20) {
        return true;
    } else {
        return false;
    }
}
/**
 * @param integer $number
 * @param string  $text
 * @return boolean
 */
function showText($number, $text): bool
{
    for ($i = 0; $i < $number; $i++) {
        echo $text . '<br>';
    }
    return true;
}

include 'showtext_form.html';

if ($count && checkNum($count_num)) {
    showText($count_num, $text);
} elseif ($count && !checkNum($count_num)) {
    echo '<span style="color: red;">Bad ' . $count_num . '</span>';
}
echo '<hr>';


// 8. Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.
$n = 12; // array length
/**
 * @param integer $length
 * @param integer $num1
 * @param integer $num2
 * @return array
 */
function createNumArr($length, $num1, $num2): array
{
    $last_num = $num1;
    $num_arr[] = $num1;
    for ($i = 0; $i < $length; $i++) {
        if ($last_num == $num1) {
            $num_arr[] = $num2;
            $last_num = $num2;
        } else {
            $num_arr[] = $num1;
            $last_num = $num1;
        }
    }
    return $num_arr;
}
dd(createNumArr($n, 0, 1));
echo '<hr>';


// 9. Определите, есть ли в массиве повторяющиеся элементы.
$arr = [43, 32, 2, 22, 43, 55, 88, 44, 44, 99, 44, 11, 22];
/**
 * @param array $array
 * @return array
 */
function findDublicate(array $array): array
{
    $new_arr = null;
    foreach ($array as $value) {
        if (!isset($new_arr[$value])) {
            $new_arr[$value] = 0;
            foreach ($array as $value2) {
                if ($value == $value2) {
                    $new_arr[$value]++;
                }
            }
        }
    }
    return $new_arr;
}
// show dublicate
foreach (findDublicate($arr) as $key => $value) {
    if ($value > 1) {
        echo 'Значение <b>' . $key . '</b> повторяется <b>' . $value . '</b> раз(а).<br>';
    }
}


// 10. Найти минимальное и максимальное среди 3 чисел
/**
 * @param mixed ...$args
 * @return string
 */
function findMinMaxNum(...$args): string
{
    $min = $args[0];
    $max = $args[0];
    foreach ($args as $value) {
        if ($value < $min) {
            $min = $value;
        } elseif ($value > $max) {
            $max = $value;
        }
    }
    return ('Min: ' . $min . ', Max: ' . $max);
}
echo '<hr>' . findMinMaxNum(432, 32, 89) . '<hr>';


// 11. Найти площадь
/**
 * @param float $a
 * @param float $b
 * @return float
 */
function calcSq($a, $b): float
{
    $s = $a * $b;
    return $s;
}
echo 'Площадь равна: ' . calcSq(43, 67) . '<hr>';


// 12. Теорема Пифагора
$gipotenuza = 15;
$katet1 = 9;
$katet2 = 7;
/**
 * @param float $c
 * @param float $a
 * @param float $b
 * @return float
 */
function calcPifagor($c, $a, $b): float
{
    if (($c * $c) == ($a * $a) + ($b * $b)) {
        return true;
    } else {
        return false;
    }
}
/**
 * @param float $a
 * @param float $b
 * @return float
 */
function findHypotenuse($a, $b): float
{
    $sum_sq = ($a * $a) + ($b * $b);
    $c = sqrt($sum_sq);
    return $c;
}

echo calcPifagor($gipotenuza, $katet1, $katet2) ? 'Да, треугольник прямоугольный<br>' : 'Нет, треугольник не прямоугольный<br>';
echo 'Гипотенуза равна: ' . findHypotenuse(12, 26) . '<hr>';


// 13. Найти периметр
/**
 * @param float $l
 * @param float $w
 * @return float
 */
function calcRectanglePerimeter($l, $w): float
{
    $P = 2 * ($l + $w);
    return $P;
}

/**
 * @param float|string $r
 * @param float|string $d
 * @return float
 */
function calcCirclePerimeter($r = '', $d = ''): float
{
    if ($r != '') {
        $P = 2 * (pi() * $r);
    } elseif ($d != '') {
        $P = pi() * $d;
    }
    return $P;
}

/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 */
function calcTrianglePerimeter($a, $b, $c): float
{
    $P = $a + $b + $c;
    return $P;
}

/**
 * @param float $a
 * @return float
 */
function calcSquarePerimeter($a): float
{
    $P = 4 * $a;
    return $P;
}

/**
 * @param float $n
 * @param float $a
 * @return float
 */
function calcPolygonPerimeter($n, $a): float
{
    $P = $n * $a;
    return $P;
}

echo 'Периметр прямоугольника равен: ' . calcRectanglePerimeter(34, 87) . '<br>';
echo 'Периметр окружности равен: ' . calcCirclePerimeter(34, '') . '<br>';
echo 'Периметр треугольника равен: ' . calcTrianglePerimeter(6, 8, 11) . '<br>';
echo 'Периметр квадрата равен: ' . calcSquarePerimeter(23) . '<br>';
echo 'Периметр правильного многоугольника равен: ' . calcPolygonPerimeter(6, 12) . '<hr>';


// 14. Найти дискриминант (Квадратное уравнение: ax2 + bx + c = 0)
/**
 * @param integer $a
 * @param integer $b
 * @param integer $c
 * @return string
 */
function calcDiscriminant($a, $b, $c): string
{
    // D = b2 - 4ac
    $D = ($b * 2) - (4 * $a * $c);
    if ($D < 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней нет';
    }
    if ($D == 0) {
        $resault = 'D = ' . $D . ' -Корень один';
    }
    if ($D > 0) {
        $resault = 'D = ' . $D . ' -Вещественных корней — два';
    }
    return $resault;
}
echo calcDiscriminant(3, 5, 8) . '<hr>';


// 15. Создать только четные числа до 100
/**
 * @param integer $max_num
 * @return boolean
 */
function showEvenNumbers($max_num): bool
{
    for ($i = 1; $i <= $max_num; $i++) {
        if (($i % 2) == 0) {
            echo $i . ', ';
        }
    }
    return true;
}
showEvenNumbers(100);
echo '<hr>';


// 16. Создать не четные числа до 100
/**
 * @param integer $max_num
 * @return boolean
 */
function showNoneEvenNumbers($max_num): bool
{
    for ($i = 1; $i <= $max_num; $i++) {
        if (($i % 2) != 0) {
            echo $i . ', ';
        }
    }
    return true;
}
showNoneEvenNumbers(100);
echo '<hr>';


// Создать функцию по нахождению числа в степени
/**
 * @param float   $num
 * @param integer $power
 * @return float
 */
function calcPow($num, $power): float
{
    return $num ** $power;
}
echo calcPow(4, 3) . '<hr>';


// написать функцию сортировки. Функция принимает массив случайных чисел и сортирует
// их по порядку. По дефолту функция сортирует в порядке возрастания. Но если передать
// второй параметр то функция будет сортировать по убыванию.
/**
 * @param integer $len
 * @param integer $num1
 * @param integer $num2
 * @return array|null
 */
function getRandNumArr($len, $num1, $num2): array
{
    $num_arr = null;
    for ($i = 0; $i < $len; $i++) {
        $num_arr[] = rand($num1, $num2);
    }
    return $num_arr;
}

/**
 * @param array       $arr
 * @param string|null $sort
 * @return array
 */
function getSortNumArr($arr, $sort = null): array
{
    if ($sort == 'asc') {
        sort($arr);
    } elseif ($sort == 'desc') {
        rsort($arr);
    } else {
        sort($arr);
    }
    return $arr;
}

$num_arr = getRandNumArr(15, 1, 100);
dd($num_arr);
$sort_num_arr = getSortNumArr($num_arr);
dd($sort_num_arr);
echo '<hr>';


// написать функцию поиска в массиве. функция будет принимать два параметра.
// Первый массив, второй поисковое число. search(arr, find)
$arr = [32, 34, 11, 18, 37, 21, 98, 88, 54, 31, 25, 21, 88];
/**
 * @param array   $arr
 * @param integer $find
 * @return boolean
 */
function searchInArray($arr, $find): bool
{
    $count = 0;
    foreach ($arr as $key => $value) {
        if ($value == $find) {
            echo 'Найдено число: ' . $value . ' с ключом: ' . $key . '<br>';
            $count++;
        }
    }
    if ($count > 0) {
        return true;
    } else {
        return false;
    }
}
searchInArray($arr, 21);
echo '<br>';
