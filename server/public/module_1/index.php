<?php
// 1
$file_paths = ['C:/Projects/something.txt', 'file.exe'];

/**
 * @param array $paths
 * @return integer
 */
function findDirectory(array $paths): int
{
    $count = 0;
    foreach ($paths as $value) {
        if (strpos($value, '/')) {
            $count++;
        }
    }
    return $count;
}
echo 'В массиве: ' . findDirectory($file_paths) . ' директорий <br>';

/**
 * @param $paths
 * @return int
 */
$findDirectory = function (array $paths): int {
    $count = 0;
    foreach ($paths as $value) {
        if (strpos($value, '/')) {
            $count++;
        }
    }
    return $count;
};
echo 'В массиве: ' . $findDirectory($file_paths) . ' директорий <br><hr>';


// 2
$nums = ['999', '999', '999'];

/**
 * @param array $num
 * @return boolean
 */
function getWinResult(array $num): bool
{
    if ($num[1] == $num[0] && $num[1] == $num[2]) {
        return true;
    } else {
        return false;
    }
}
// phpcs:disable
var_dump(getWinResult($nums));
// phpcs:enable
echo '<br>';

/**
 * @param array $num
 * @return boolean
 */
$getWinResult = function (array $num): bool {
    if ($num[1] == $num[0] && $num[1] == $num[2]) {
        return true;
    } else {
        return false;
    }
};
// phpcs:disable
var_dump($getWinResult($nums));
// phpcs:enable
echo '<br>';

/**
 * @param array $num
 * @return boolean
 */
$getWinResult = fn(array $num): bool => $num[1] == $num[0] && $num[1] == $num[2];
// phpcs:disable
var_dump($getWinResult($nums));
// phpcs:enable
echo '<br>';


// 3
$names = ['Sam', 'Frodo', 'Troll', 'Balrog', 'Human'];
$names2 = ['Orc', 'Frodo', 'Treant', 'Saruman', 'Sam'];
$names3 = ['Orc', 'Sam', 'Frodo', 'Gandalf', 'Legolas'];

/**
 * @param array $allNames
 * @return boolean
 */
function isFriendsTogether(array $allNames): bool
{
    foreach ($allNames as $key => $value) {
        if ($value == 'Sam' && $key != 0 && $allNames[--$key] == 'Frodo' ||
            $value == 'Frodo' && $key != 0 && $allNames[--$key] == 'Sam') {
            return true;
        }
    }
    return false;
}
// phpcs:disable
var_dump(isFriendsTogether($names3));
// phpcs:enable
echo '<br>';


// 4
$nums = [1, 2, 3];
$nums2 = [1, -2, 3];
$nums3 = [0, 0, 10];
$nums4 = [-1, -2, -3];

/**
 * @param array $nums_arr
 * @return integer
 */
function findSecondBigNum(array $nums_arr): int
{
    $num1 = $nums_arr[0];
    $num2 = $nums_arr[0];
    for ($i = 0; $i < 3; $i++) {
        if ($i == 0) {
            if ($nums_arr[$i] > $num1) {
                $num1 = $nums_arr[$i];
            }
        } elseif ($i == 1) {
            if ($nums_arr[$i] > $num2) {
                $num2 = $nums_arr[$i];
            }
        }
    }
    return $num2;
}
echo 'Второе максимальное число: ' . findSecondBigNum($nums4) . '<br><hr>';


// 5
$arr_str = ['in', 'Soviet', 'Russia', 'frontend', 'programms', 'you'];
$arr_str2 = ['using','clojure','makes','your','life','greater'];

/**
 * @param array $str
 * @return array
 */
function findLongString(array $str): array
{
    $len_str = 0;
    $len_str_new = 0;
    foreach ($str as $value) {
        $len_str_new = strlen($value);
        if ($len_str_new > $len_str) {
            $len_str = $len_str_new;
            $long_str_arr = [$value];
        } elseif ($len_str_new == $len_str) {
            $long_str_arr[] = $value;
        }
    }
    return $long_str_arr;
}
// phpcs:disable
print_r(findLongString($arr_str2));
// phpcs:enable
echo '<hr>';


// 6
$nums = [90, 91, 99, 93, 100];

/**
 * @param array $array
 * @return string
 */
function avrgNums(array $array): string
{
    $avrg_nums = 0;
    $count = 0;
    foreach ($array as $value) {
        $avrg_nums += $value;
        $count++;
    }
    $avrg = $avrg_nums / $count;

    if ($avrg > 90) {
        $result = 'Grade: A';
    } elseif ($avrg > 80) {
        $result = 'Grade: B';
    } elseif ($avrg > 70) {
        $result = 'Grade: C';
    } elseif ($avrg > 60) {
        $result = 'Grade: D';
    } else {
        $result = 'Grade: E';
    }
    return $result;
}
echo avrgNums($nums) . '<br><hr>';


// 7
/**
 * @param integer $num
 * @return string
 */
function getFigure($num): string
{
    switch ($num) {
        case '1':
            $figure = 'circle';
            break;
        case '2':
            $figure = 'semi-circle';
            break;
        case '3':
            $figure = 'triangle';
            break;
        case '4':
            $figure = 'square';
            break;
        case '5':
            $figure = 'pentagon';
            break;
        case '6':
            $figure = 'hexagon';
            break;
        case '7':
            $figure = 'heptagon';
            break;
        case '8':
            $figure = 'octagon';
            break;
        case '9':
            $figure = 'nonagon';
            break;
        case '10':
            $figure = 'decagon';
            break;
        default:
            $figure = 'none';
    }
    return $figure;
}
echo getFigure(1) . '<br><hr>';


// 8
$animals = [2, 3, 5];

/**
 * @param array $arr
 * @return integer
 */
$legs = fn(array $arr): int => ($arr[0] * 2) + ($arr[1] * 4) + ($arr[2] * 4);
echo $legs($animals) . '<br><hr>';


// 9
$wordsArr = ['hello', 'world'];

/**
 * @param array $words
 * @return array
 */
function wordsTransform(array $words): array
{
    foreach ($words as $value) {
        $new_arr[] = strlen($value);
    }
    return $new_arr;
}
// phpcs:disable
print_r(wordsTransform($wordsArr));
// phpcs:enable
echo '<br><hr>';


// 10
$word = 'forks';

/**
 * @param string $item
 * @return boolean
 */
$isMany = fn(string $item): bool => $item[strlen($item) - 1] == 's';
// phpcs:disable
var_dump($isMany($word));
// phpcs:enable