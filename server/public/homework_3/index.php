<?php

// Part *
// 1
$a = 42;
$b = 55;
echo $a < $b ? 'число ' . $b . ' больше чем ' . $a : ' число ' . $a . ' больше чем ' . $b;
echo '<br>';

// 2
$a = rand(5, 15);
$b = rand(5, 15);
echo $a < $b ? 'число ' . $b . ' больше чем ' . $a : 'число ' . $a . ' больше чем ' . $b;
echo '<br>';

// 3
$name = 'Oleg';
$surname = 'Alekhnovich';
$patronymic = 'Andreyevich';
echo 'Full name: ' . $surname . ' ' . $name[0] . ' ' . $patronymic[0];
echo '<br>';

// 4
$num = 3;
$rand_num = rand(1, 99999);
$count_num = substr_count($rand_num, $num);
echo 'число ' . $num . ' встречается ' . $count_num . ' раз(а) в числе ' . $rand_num;
echo '<br>';

// 5
$a = 3;
echo $a . '<br>';

$a = 10;
$b = 2;
echo 'Сумма: ' . ($a + $b) . ' Разность: ' . ($a - $b) . ' Произведение: ' . ($a * $b) . ' Частное: ' . ($a / $b);
echo '<br>';

$c = 15;
$d = 2;
$result = $c + $d;
echo $result;
echo '<br>';

$a = 10;
$b = 2;
$c = 5;
echo $a + $b + $c;
echo '<br>';

// 6
$a = 17;
$b = 10;
$c = $a - $b;
$d = $c;
$result = $c + $d;
echo $result;
echo '<br>';

// 7
$text = 'Привет, Мир!';
echo $text;
echo '<br>';

$text1 = 'Привет, ';
$text2 = 'Мир!';
echo $text1 . $text2 . '<br>';

$hour = 60; // 1 час = 60 минут
$sec_in_1hour = 60 * $hour;
$sec_in_1day = 24 * $sec_in_1hour;
$sec_in_1week = 7 * $sec_in_1day;
$sec_in_1month = 30 * $sec_in_1day;
echo 'Секунд в 1 часе: ' . $sec_in_1hour  . ' Секунд в сутках: ' . $sec_in_1day . ' Секунд в неделе: ' . $sec_in_1week . ' Секунд в 1 месяце (30 дней): ' . $sec_in_1month;
echo '<br>';

// 8
$var = 1;
$var += 12;
$var -= 14;
$var *= 5;
$var /= 7;
++$var;
--$var;
echo $var;


// Part **
// 1
$hour = date('H');
$min = date('i');
$sec = date('s');
echo '<br>' . '$hour:$min:$sec' . '<br>';

// 2
$text = 'Я';
$text .= ' хочу';
$text .= ' знать';
$text .= ' PHP!';
echo $text;

// 3
$foo = 'bar';
$bar = 10;
echo '<br>' . $$foo . '<br>';

// 4
$a = 2;
$b = 4;
echo ($a++) + ($b) . ', '; // 6
echo ($a) + (++$b) . ', '; // 8
echo (++$a) + ($b++); // 9

// 5
$a = 'Hi all';
echo '<br>';
echo isset($a) ? 'переменная существует' : 'переменная не существует';

$a = 1;
echo '<br>';
echo gettype($a) == 'integer' ? 'тип переменной integer' : 'тип переменной не integer';

$a = null;
echo '<br>';
echo is_null($a) ? 'переменная со значением NULL' : 'переменной присвоено значение';

$a = 32;
echo '<br>';
echo is_integer($a) ? 'переменная целое число' : 'переменная не целое число';

$a = 40.2;
echo '<br>';
echo is_double($a) ? 'переменная число с плавающей точкой' : 'переменная не число с плавающей точкой';

$a = 'text';
echo '<br>';
echo is_string($a) ? 'переменная строка' : 'переменная не строка';

$a = '154';
echo '<br>';
echo is_numeric($a) ? 'переменная число' : 'переменная не число';

$a = true;
echo '<br>';
echo is_bool($a) ? 'булевая переменная' : 'не булевая переменная';

$a = true;
echo '<br>';
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 45.56778;
echo '<br>';
echo is_scalar($a) ? 'переменная со скалярным значением' : 'переменная не со скалярным значением';

$a = 'abc';
echo '<br>';
echo is_null($a) ? 'переменная NULL' : 'переменная не NULL';

$a = ['a', 'b', 'c'];
echo '<br>';
echo is_array($a) ? 'переменная массив' : 'переменная не массив';

$a = (object)'name';
echo '<br>';
echo is_object($a) ? 'переменная объект' : 'переменная не объект';

echo '<br>';

// Part ***
// 1
$x = 21;
$y = 45;
echo $x + $y;
echo '<br>';

// 2
$x = 87;
$y = 24;
echo ($x ** 2) + ($y ** 2);
echo '<br>';

// 3
$arr = [3, 6, 24];
$arr_count = count($arr);
echo array_sum($arr) / $arr_count;
echo '<br>';

// 4
$x = 3;
$y = 7;
$z = 5;
echo (++$x) - (2 * ($z - ($x * 2) + $y));
echo '<br>';

// 5
$x = 254;
echo $x % 3 . '<br>';
echo $x % 5 . '<br>';

$y = 50;
echo ($y * 0.3) + $y . '<br>'; // на 30%
echo ($y * 1.2) + $y . '<br>'; // на 120%

// 6
$x = 80;
$y = 150;
echo ($x * 0.4) + ($y * 0.85) . '<br>';

$z = 345;
$num_arr = str_split($z);
echo array_sum($num_arr) . '<br>';

// 7
$n = 856;
$n = (string)$n;
$n[1] = 0;
echo (integer)$n . '<br>';
$arr_num = array_reverse(str_split($n));
echo (integer)implode('', $arr_num) . '<br>';

// 8
$n = 545;
echo ($n % 2) == 0 ? 'четное число' : 'нечетное число';
