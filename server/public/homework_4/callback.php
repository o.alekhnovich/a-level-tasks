<?php
session_start();
// Gen token
$token = '';
if (isset($_SESSION['token']) && $_SESSION['token'] != '') {
    $token = $_SESSION['token'];
} else {
    $_SESSION['token'] = hash('sha512', uniqid());
}

echo '<h1>Заказать обратный звонок</h1>';

if (isset($_POST['name']) && $_POST['name'] != '' &&
    isset($_POST['phone']) && $_POST['phone'] != ''
    ) {
    if (isset($_POST['token']) && $_POST['token'] == $token) {
        $name = htmlspecialchars($_POST['name']);
        $phone = $_POST['phone'];

        echo 'Здравствуйте ' . $name . '. Вы успешно заказали обратный звонок на номер: ' . $phone;

        $token = hash('sha512', uniqid());
        $_SESSION['token'] = $token;
    } else {
        echo '<p style="color: red">Не правильный токен сессии</p>';
    }
} else {
    echo '* Все необходимые поля должны быть заполнены.';
}

include 'callback_form.html';
