<?php
session_start();

// delete all items from cart
if (isset($_POST['cart_empty']) && $_POST['cart_empty'] == 'del') {
    unset($_SESSION['cart']);
}

// add item to the cart
if (isset($_POST['id']) && $_POST['id'] != '' &&
    isset($_POST['count']) && $_POST['count'] != ''
) {
    $item_id = $_POST['id'];
    $item_count = $_POST['count'];

    $_SESSION['cart'][$item_id] = $item_count;
}

// show items in the cart
if (isset($_SESSION['cart']) && $_SESSION['cart'] != '') {
    echo 'В корзине есть товары<br>';
    // phpcs:disable
    echo '<pre>';
    print_r($_SESSION);
    echo '</pre>';
    // phpcs:enable
} else {
    echo 'Ваша корзина пуста';
}

include 'cart_form.html';
