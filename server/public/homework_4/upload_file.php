<?php
$valid_types = [
    'pdf' => 'application/pdf',
    'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'xls' => 'application/vnd.ms-excel',
    // images
    'png' => 'image/png',
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
];

if (isset($_FILES['userfile'])) {
    if (isset($_FILES['userfile']['tmp_name'])) {
        $filename = basename($_FILES['userfile']['name']);
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/homework_4/upload/';
        $uploadfile = $uploaddir . $filename;

        $arr = explode('.', $filename);
        $file_type = strtolower(array_pop($arr));

        if (array_key_exists($file_type, $valid_types)) {
            if ($valid_types[$file_type] == $_FILES['userfile']['type']) {
                move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);
                echo 'file uploaded success';
            } else {
                echo 'wrong mime type: ' . $_FILES['userfile']['type'];
            }
        } else {
            echo 'wrong file type';
        }
    } else {
        echo 'empty file name on server';
    }
} else {
    echo 'file is not selected';
}

include 'upload_form.html';
