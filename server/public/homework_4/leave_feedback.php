<?php

echo '<h1>Оставьте свой отзыв</h1>';

if (isset($_POST['name']) && $_POST['name'] != '' &&
    isset($_POST['feedback']) && $_POST['feedback'] != ''
    ) {
    echo '<div style="border: 1px solid #999; padding: 10px;">';
    echo htmlspecialchars($_POST['name']) . '<br>' . htmlspecialchars($_POST['feedback']);
    echo '</div>';
} else {
    echo '* Заполните все поля';
}

include 'feedback_form.html';
