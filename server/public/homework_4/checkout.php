<?php
session_start();
// session_destroy();

// items ID list (ID => count)
$_SESSION['checkout_cart'][342345] = 1;
$_SESSION['checkout_cart'][523245] = 2;
$_SESSION['checkout_cart'][963581] = 1;
$_SESSION['checkout_cart'][995664] = 4;
$_SESSION['checkout_cart'][123221] = 1;

echo '<b>Информация о покупке:</b><br>';

if (isset($_POST['items']) && $_POST['items'] != '' &&
    isset($_POST['name']) && $_POST['name'] != '' &&
    isset($_POST['phone']) && $_POST['phone'] != ''
) {
    echo 'Имя покупателя: ' . htmlspecialchars($_POST['name']) . '<br>';
    echo 'Телефон покупателя: ' . $_POST['phone'] . '<br>';
    echo 'Доставка: ' . $_POST['delivery'] . '<br>';
    echo 'Адрес доставки: ' . htmlspecialchars($_POST['address']) . '<br>';
    foreach ($_POST['items'] as $id => $count) {
        echo 'ID товара: ' . $id . ' (Кол-во: ' . $count . ') <br>';
    }
}

echo '<hr><h1>Оформление заказа</h1>';

echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">';

foreach ($_SESSION['checkout_cart'] as $id => $count) {
    echo '<input type="hidden" name="items[' . $id . ']" value="' . $count . '" multiple>';
}

include 'checkout_form.html';

echo '</form>';
